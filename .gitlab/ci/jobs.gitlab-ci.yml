include:
  - template: Dependency-Scanning.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml
  - template: Container-Scanning.gitlab-ci.yml

# ======================================================================================================================
# Runners
# ======================================================================================================================
.gem_cache: &gem_cache
  key:
    files:
      - Gemfile.lock
  paths:
    - vendor/bundle
  policy: pull

.coverage_cache: &coverage_cache
  key: coverage-$CODACY_VERSION
  paths:
    - codacy-coverage-reporter-$CODACY_VERSION
  policy: pull

.ruby_runner:
  image: ruby:2.7-bullseye
  variables:
    BUNDLE_PATH: vendor/bundle
    BUNDLE_FROZEN: "true"
    BUNDLE_SUPPRESS_INSTALL_USING_MESSAGES: "true"
  before_script:
    - bundle install
  cache:
    - *gem_cache
  interruptible: true

.buildkit_runner:
  image:
    name: ${BUILDKIT_IMAGE}
    entrypoint: [""]
  before_script:
    - |
      mkdir -p $HOME/.docker
      cat <<- EOF > $HOME/.docker/config.json
      {
        "auths": {
          "registry.gitlab.com": {
            "auth": "$(echo -n $CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD | base64)"
          }
        }
      }
      EOF
  script:
    - .gitlab/script/build-image.sh
  retry: 2
  interruptible: true

.docker_runner:
  image: ${CI_IMAGE}
  services:
    - name: ${DIND_IMAGE}
      alias: docker
  variables:
    DOCKER_HOST: tcp://docker:2376
    DOCKER_TLS_CERTDIR: /certs
    DOCKER_CERT_PATH: /certs/client
    DOCKER_TLS_VERIFY: 1
    DOCKER_BUILDKIT: 1
    DOCKER_CREDENTIALS: $DOCKERHUB_USERNAME:$DOCKERHUB_PASSWORD
    DOCKER_REGISTRY: https://index.docker.io/v1/
  before_script:
    - '[[ "$CI_JOB_NAME" != "release-image" ]] || echo "$DOCKERHUB_PASSWORD" | docker login -u $DOCKERHUB_USERNAME --password-stdin'
    - while ! test -f "$DOCKER_CERT_PATH/ca.pem"; do sleep 2; done
  interruptible: true

# ======================================================================================================================
# Jobs
# ======================================================================================================================

# ----------------------------------------------------------------------------------------------------------------------
# .pre stage
#
.cache_dependencies:
  stage: .pre
  extends: .ruby_runner
  script:
    - .gitlab/script/download-coverage.sh
  cache:
    - <<: *gem_cache
      policy: pull-push
    - <<: *coverage_cache
      policy: pull-push

# ----------------------------------------------------------------------------------------------------------------------
# build stage
#
.build_app_image:
  stage: build
  extends: .buildkit_runner
  needs: []

.build_ci_image:
  stage: build
  extends: .buildkit_runner
  needs: []
  variables:
    DOCKER_FILE: .gitlab/docker/ci
    DOCKER_IMAGE: ci

# ----------------------------------------------------------------------------------------------------------------------
# 'static analysis' stage
#
.rubocop:
  stage: static analysis
  extends: .ruby_runner
  script:
    - bundle exec rubocop --parallel --color

.reek:
  stage: static analysis
  extends: .ruby_runner
  script:
    - bundle exec reek --color --progress --force-exclusion --sort-by smelliness .

.brakeman:
  stage: static analysis
  extends: brakeman-sast

.bundle_audit:
  stage: static analysis
  extends: bundler-audit-dependency_scanning

.dependency_scan:
  stage: static analysis
  extends: gemnasium-dependency_scanning
  variables:
    DS_REMEDIATE: "false"

.container_scanning:
  stage: static analysis
  extends: container_scanning
  variables:
    DOCKER_IMAGE: $APP_IMAGE

# ----------------------------------------------------------------------------------------------------------------------
# test stage
#
.rspec:
  stage: test
  extends: .ruby_runner
  services:
    - name: bitnami/redis:6.2-debian-10
      alias: redis
    - name: bitnami/mongodb:4.4-debian-10
      alias: mongodb
  variables:
    REDIS_URL: redis://redis:6379
    REDIS_PASSWORD: $REDIS_PASSWORD
    MONGODB_URL: mongodb:27017
    COVERAGE: "true"
    MAX_ROWS: 5
    OUTPUT_STYLE: block
    NO_COLOR: 1
  coverage: /^COVERAGE:\s+(\d{1,3}\.\d{1,2})\%/
  script:
    - bundle exec rspec --format documentation --format RspecJunitFormatter --out tmp/rspec.xml
  after_script:
    - ./codacy-coverage-reporter-$CODACY_VERSION report -r coverage/coverage.xml
  cache:
    - *gem_cache
    - *coverage_cache
  artifacts:
    reports:
      cobertura: coverage/coverage.xml
      junit: tmp/rspec.xml
    paths:
      - reports/allure-results
    expire_in: 1 day
    when: always

.standalone:
  stage: test
  extends: .docker_runner
  variables:
    COMPOSE_PROJECT_NAME: dependabot
  script:
    - .gitlab/script/run-standalone.sh
  after_script:
    - curl -X POST -s "http://docker:8081/sessions/verify" | jq

# ----------------------------------------------------------------------------------------------------------------------
# test report stage
#
.allure_report:
  stage: report
  image:
    name: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/andrcuns/allure-report-publisher:0.4.1
    entrypoint: [""]
  variables:
    GITLAB_AUTH_TOKEN: $GITLAB_ACCESS_TOKEN
    ALLURE_JOB_NAME: rspec
    GOOGLE_CLOUD_KEYFILE_JSON: $KEYFILE_TEST_REPORTS
  script:
    - |
      allure-report-publisher upload gcs \
        --results-glob="reports/allure-results/*" \
        --bucket="allure-test-reports" \
        --prefix="dependabot-gitlab/$CI_COMMIT_REF_SLUG" \
        --update-pr="comment" \
        --copy-latest \
        --color

# ----------------------------------------------------------------------------------------------------------------------
# release stage
#
.release_image:
  stage: release
  extends: .docker_runner
  variables:
    RELEASE_IMAGE: docker.io/andrcuns/dependabot-gitlab
  script:
    - export RELEASE_VERSION=$(echo $CI_COMMIT_TAG | grep -oP 'v\K[0-9.]+')
    - docker pull "$APP_IMAGE"
    - docker tag "$APP_IMAGE" "$RELEASE_IMAGE:$RELEASE_VERSION"
    - docker tag "$APP_IMAGE" "$RELEASE_IMAGE:latest"
    - docker push "$RELEASE_IMAGE:$RELEASE_VERSION" && docker push "$RELEASE_IMAGE:latest"
  interruptible: false

.gitlab_release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  variables:
    RELEASE_NOTES_FILE: release_notes.md
    SETTINGS__GITLAB_ACCESS_TOKEN: $GITLAB_ACCESS_TOKEN
  before_script:
    - apk --no-cache add -q jq bash curl grep
  script:
    - .gitlab/script/changelog.sh
  release:
    tag_name: $CI_COMMIT_TAG
    description: $RELEASE_NOTES_FILE
  interruptible: false

.update_chart:
  extends: .ruby_runner
  stage: release
  variables:
    SETTINGS__GITLAB_ACCESS_TOKEN: $GITLAB_ACCESS_TOKEN
  script:
    - bundle exec rake "release:chart[$CI_COMMIT_TAG]"
  interruptible: false

.update_standalone:
  extends: .ruby_runner
  stage: release
  variables:
    SETTINGS__GITLAB_ACCESS_TOKEN: $GITLAB_ACCESS_TOKEN
  script:
    - bundle exec rake "release:standalone[$CI_COMMIT_TAG]"
  interruptible: false

# ----------------------------------------------------------------------------------------------------------------------
# deploy stage
#
.deploy:
  stage: deploy
  trigger:
    project: dependabot-gitlab/deploy
    strategy: depend
